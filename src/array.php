<?php

/**
 * @package AvePhpExt\Arrays
 * @author Averor <averor.dev@gmail.com>
 */

namespace AvePhpExt\Arrays {

    if (!function_exists('array_recursive_ksort')) {

        /**
         * Recursively sorts array by key
         *
         * @param array $array
         * @return bool
         */
        function array_recursive_ksort(array &$array) : bool
        {
            foreach ($array as &$value) {
                if (is_array($value)) {
                    array_recursive_ksort($value);
                }
            }

            return ksort($array);
        }
    }

    if (!function_exists('array_unset_value')) {

        /**
         * Remove array element(s) by value, without reindexing
         *
         * @param array $array
         * @param mixed $value
         * @param bool $strict
         * @return void
         */
        function array_unset_value(array &$array, $value, $strict = false)
        {
            $array = array_filter(
                $array,
                function ($i) use ($value, $strict) {
                    return $strict
                        ? $i !== $value
                        : $i != $value;
                }
            );
        }
    }

    if (!function_exists('array_flatten')) {

        /**
         * Flatten multidimensional array
         *
         * @param array $array
         * @param int $maxDepth
         * @param bool $preserveKeys
         * @return array
         */
        function array_flatten(array $array, int $maxDepth = -1, bool $preserveKeys = false) : array
        {
            // setting -2 as unlimited due to achieve intuitive function behavior:
            // -1 as unlimited and 0-n as array depth level (similar to RecursiveIteratorIterator::depth)
            // Note that, because of this, setting $maxDepth to 0 (zero) causes no flattening being made
            $maxDepth = $maxDepth == -1
                ? -2
                : $maxDepth - 1;

            $doFlatten = function (array $array, int $depth = 0, array &$return) use (
                &$doFlatten,
                $maxDepth,
                $preserveKeys
            ) : array {
                foreach ($array as $key => $value) {
                    if (is_array($value) && ($maxDepth === -2 || $depth <= $maxDepth)) {
                        $doFlatten(
                            $value,
                            $depth + 1,
                            $return
                        );
                    } else {
                        if ($preserveKeys) {
                            $return[$key] = $value;
                        } else {
                            $return[] = $value;
                        }
                    }
                }

                return $return;
            };

            $return = [];

            return $doFlatten(
                $array,
                0,
                $return
            );
        }
    }
}
