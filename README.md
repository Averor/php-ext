# Averor/php-ext
Collections of miscellaneous functions, extending core php functionalities

## Currently available functions:

### Arrays

###### *Recursively sorts array by key*
```php
bool array_recursive_ksort (array &$array)
```

###### *Flatten multidimensional array*
```php
array array_flatten (array $array [, int $maxDepth = -1 [, bool $preserveKeys = false]])
```

###### *Remove array element(s) by value*
```php
void array_unset_value (array &$array, mixed $value [, bool $strict = false])
```