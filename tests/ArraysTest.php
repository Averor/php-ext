<?php

namespace AvePhpExt\Tests;

use function AvePhpExt\Arrays\array_recursive_ksort;
use function AvePhpExt\Arrays\array_unset_value;
use function AvePhpExt\Arrays\array_flatten;

/**
 * Class ArraysTest
 *
 * @package AvePhpExt\Tests
 * @author Averor <averor.dev@gmail.com>
 */
class ArraysTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public static function providesValidValuesForArrayRecursiveKsort()
    {
        return [

            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                ],
                // expected
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                ],
            ],

            // #2
            [
                // test
                [
                    'p2' => 'v2',
                    'p1' => 'v1',
                ],
                // expected
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                ],
            ],

            // #3
            [
                // test
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => 'v13',
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ],
                // expected
                [
                    'p1' => [
                        'p11' => 'v11',
                        'p12' => 'v12',
                        'p13' => 'v13',
                    ],
                    'p2' => 'v2',
                    'p3' => 'v3',
                ]

            ],

            // #4
            [
                // test
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => [
                            'p131' => 'v131',
                            'p133' => 'v133',
                            'p132' => 'v132',
                        ],
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ],
                // expected
                [
                    'p1' => [
                        'p11' => 'v11',
                        'p12' => 'v12',
                        'p13' => [
                            'p131' => 'v131',
                            'p132' => 'v132',
                            'p133' => 'v133',
                        ],
                    ],
                    'p2' => 'v2',
                    'p3' => 'v3',
                ]

            ],

        ];
    }

    /**
     * @return array
     */
    public static function providesInvalidValuesForArrayRecursiveKsort()
    {
        return [

            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                ],
                // erroneous
                [
                    'p2' => 'v2',
                    'p1' => 'v1',
                ],
            ],

            // #2
            [
                // test
                [
                    'p2' => 'v2',
                    'p1' => 'v1',
                ],
                // erroneous (same as tested)
                [
                    'p2' => 'v2',
                    'p1' => 'v1',
                ],
            ],

            // #3
            [
                // test
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => 'v13',
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ],
                // erroneous (same as tested)
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => 'v13',
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ]

            ],

            // #4
            [
                // test
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => [
                            'p131' => 'v131',
                            'p133' => 'v133',
                            'p132' => 'v132',
                        ],
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ],
                // erroneous (same as tested)
                [
                    'p3' => 'v3',
                    'p1' => [
                        'p12' => 'v12',
                        'p13' => [
                            'p131' => 'v131',
                            'p133' => 'v133',
                            'p132' => 'v132',
                        ],
                        'p11' => 'v11',
                    ],
                    'p2' => 'v2',
                ]

            ],

        ];
    }

    /**
     * @return array
     */
    public function providesValidResultsForArrayUnsetValueStrict()
    {
        return [
            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v3',
                ],
                // value to remove
                'v2',
                // expected
                [
                    'p1' => 'v1',
                    'p3' => 'v3',
                ]
            ],

            // #2
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ],
                // value to remove
                'v2',
                // expected
                [
                    'p1' => 'v1',
                ]
            ],

            // #3
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                2,
                // expected
                [
                    'p1' => 1,
                    'p3' => 3,
                ],
            ],

            // #4
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                '2',
                // expected
                [
                    'p1' => '1',
                    'p3' => '3',
                ],
            ],

            // #5
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                '2',
                // expected
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
            ],

            // #6
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                2,
                // expected
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function providesValidResultsForArrayUnsetValueNonStrict()
    {
        return [
            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v3',
                ],
                // value to remove
                'v2',
                // expected
                [
                    'p1' => 'v1',
                    'p3' => 'v3',
                ]
            ],

            // #2
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ],
                // value to remove
                'v2',
                // expected
                [
                    'p1' => 'v1',
                ]
            ],

            // #3
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                2,
                // expected
                [
                    'p1' => 1,
                    'p3' => 3,
                ],
            ],

            // #4
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                '2',
                // expected
                [
                    'p1' => '1',
                    'p3' => '3',
                ],
            ],

            // #5
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                '2',
                // expected
                [
                    'p1' => 1,
                    'p3' => 3,
                ],
            ],

            // #6
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                2,
                // expected
                [
                    'p1' => '1',
                    'p3' => '3',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function providesInvalidResultsForArrayUnsetValueStrict()
    {
        return [
            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v3',
                ],
                // value to remove
                'v2',
                // erroneous
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v3',
                ]
            ],

            // #2
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ],
                // value to remove
                'v2',
                // erroneous
                [
                    'p1' => 'v1',
                    'p3' => 'v2',
                ]
            ],

            // #3
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ],
                // value to remove
                'v2',
                // erroneous
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                ]
            ],

            // #4
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ],
                // value to remove
                'v2',
                // erroneous
                [
                    'p1' => 'v1',
                    'p2' => 'v2',
                    'p3' => 'v2',
                ]
            ],

            // #5
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                '2',
                // erroneous
                [
                    'p1' => 1,
                    'p3' => 3,
                ],
            ],

            // #6
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                2,
                // erroneous
                [
                    'p1' => '1',
                    'p3' => '3',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function providesInvalidResultsForArrayUnsetValueNonStrict()
    {
        return [
            // #1
            [
                // test
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
                // value to remove
                '2',
                // erroneous
                [
                    'p1' => 1,
                    'p2' => 2,
                    'p3' => 3,
                ],
            ],

            // #2
            [
                // test
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
                // value to remove
                2,
                // erroneous
                [
                    'p1' => '1',
                    'p2' => '2',
                    'p3' => '3',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function providesValidResultsForArrayFlatten()
    {
        return [

            // #1
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                -1,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => 'v221',
                    3 => 'v222',
                    4 => 'v23',
                    5 => 'v3',
                ]
            ],

            // #2
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                1,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => [
                        'p221' => 'v221',
                        'p222' => 'v222',
                    ],
                    3 => 'v23',
                    4 => 'v3',
                ]
            ],

            // #3
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                -1,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v1',
                    'p21' => 'v21',
                    'p221' => 'v221',
                    'p222' => 'v222',
                    'p23' => 'v23',
                    'p3' => 'v3',
                ]
            ],

            // #4
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                1,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v1',
                    'p21' => 'v21',
                    'p22' => [
                        'p221' => 'v221',
                        'p222' => 'v222',
                    ],
                    'p23' => 'v23',
                    'p3' => 'v3',
                ]
            ],

            // #5
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                2,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => 'v221',
                    3 => 'v222',
                    4 => 'v23',
                    5 => 'v3',
                ]
            ],

            // #6
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p21' => 'v21',
                        'p22' => [
                            'p221' => 'v221',
                            'p222' => 'v222',
                        ],
                        'p23' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                2,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v1',
                    'p21' => 'v21',
                    'p221' => 'v221',
                    'p222' => 'v222',
                    'p23' => 'v23',
                    'p3' => 'v3',
                ]
            ],

            // #7
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                -1,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => 'v221',
                    3 => 'v222',
                    4 => 'v23',
                    5 => 'v3',
                ]
            ],

            // #8
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                -1,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v221',
                    'p2' => 'v222',
                    'p3' => 'v3',
                ]
            ],

            // #9
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                1,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => [
                        'p1' => 'v221',
                        'p2' => 'v222',
                    ],
                    3 => 'v23',
                    4 => 'v3',
                ]
            ],

            // #10
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                1,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v21',
                    'p2' => [
                        'p1' => 'v221',
                        'p2' => 'v222',
                    ],
                    'p3' => 'v3',
                ]
            ],

            // #11
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                2,
                // whether to preserve keys
                false,
                // expected
                [
                    0 => 'v1',
                    1 => 'v21',
                    2 => 'v221',
                    3 => 'v222',
                    4 => 'v23',
                    5 => 'v3',
                ]
            ],

            // #12
            [
                // test
                [
                    'p1' => 'v1',
                    'p2' => [
                        'p1' => 'v21',
                        'p2' => [
                            'p1' => 'v221',
                            'p2' => 'v222',
                        ],
                        'p3' => 'v23',
                    ],
                    'p3' => 'v3',
                ],
                // max depth (-1 = unlimited)
                2,
                // whether to preserve keys
                true,
                // expected
                [
                    'p1' => 'v221',
                    'p2' => 'v222',
                    'p3' => 'v3',
                ]
            ],

        ];
    }

    /**
     * @dataProvider providesValidValuesForArrayRecursiveKsort
     *
     * @param array $tested
     * @param array $expected
     */
    public function testValidArrayRecursiveKsort(array $tested, array $expected)
    {
        array_recursive_ksort($tested);

        $this->assertSame(
            $expected,
            $tested
        );
    }

    /**
     * @dataProvider providesInvalidValuesForArrayRecursiveKsort
     *
     * @param array $tested
     * @param array $expected
     */
    public function testInvalidArrayRecursiveKsort(array $tested, array $expected)
    {
        array_recursive_ksort($tested);

        $this->assertNotSame(
            $expected,
            $tested
        );
    }

    /**
     * @dataProvider providesValidResultsForArrayUnsetValueStrict
     *
     * @param array $array
     * @param mixed $value
     * @param array $expected
     */
    public function testValidResultsForArrayUnsetValueStrict(array $array, $value, array $expected)
    {
        array_unset_value($array, $value, true);

        $this->assertSame($expected, $array);
    }

    /**
     * @dataProvider providesValidResultsForArrayUnsetValueNonStrict
     *
     * @param array $array
     * @param mixed $value
     * @param array $expected
     */
    public function testValidResultsForArrayUnsetValueNonStrict(array $array, $value, array $expected)
    {
        array_unset_value($array, $value, false);

        $this->assertSame($expected, $array);
    }

    /**
     * @dataProvider providesInvalidResultsForArrayUnsetValueStrict
     *
     * @param array $array
     * @param mixed $value
     * @param array $expected
     */
    public function testInvalidResultsForArrayUnsetValueStrict(array $array, $value, array $expected)
    {
        array_unset_value($array, $value, true);

        $this->assertNotSame($expected, $array);
    }

    /**
     * @dataProvider providesInvalidResultsForArrayUnsetValueNonStrict
     *
     * @param array $array
     * @param mixed $value
     * @param array $expected
     */
    public function testInvalidResultsForArrayUnsetValueNonStrict(array $array, $value, array $expected)
    {
        array_unset_value($array, $value, false);

        $this->assertNotSame($expected, $array);
    }

    /**
     * @dataProvider providesValidResultsForArrayFlatten
     *
     * @param array $array
     * @param int $maxDepth
     * @param bool $preserveKeys
     * @param array $expected
     */
    public function testArrayFlatten(array $array, int $maxDepth, bool $preserveKeys, array $expected)
    {
        $this->assertSame(
            $expected,
            array_flatten($array, $maxDepth, $preserveKeys)
        );
    }
}
